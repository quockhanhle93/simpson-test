package com.simpson.dice;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import com.simpson.utils.Constants;
import com.simpson.utils.PropertiesUtils;

/**
 * An object of class PairOfDice represents a pair of dice, where each die shows
 * a number between 1 and 6. The dice can be rolled, which randomizes the
 * numbers showing on the dice.
 * 
 * @author quockhanhle93
 */

public class PairOfDice {
	public static Properties properties = PropertiesUtils.getInstance();

	private int die1; // Number showing on the first die.
	private int die2; // Number showing on the second die.
	private int factor;
	private int favoredFace;
	int rollingTime = Integer.valueOf(properties.get(Constants.ROLLING_TIME).toString());

	public Map<Integer, Double> discreteProb = new HashMap<>(6);

	public PairOfDice() {
	}

	private void loadProperties(int die) {
		if (die == 1) {
			favoredFace = Integer.valueOf(properties.get(Constants.DIE1_FAVORED_FACE).toString());
			factor = Integer.valueOf(properties.get(Constants.DIE1_FAVORED_FACTOR).toString());
		} else {
			favoredFace = Integer.valueOf(properties.get(Constants.DIE2_FAVORED_FACE).toString());
			factor = Integer.valueOf(properties.get(Constants.DIE2_FAVORED_FACTOR).toString());
		}
	}

	/**
	 * 
	 * @param (int)
	 *            die
	 * @return the array of output faces based on their probability
	 */
	public int[] roll(int die) {
		// Roll the dice by setting each of the dice to be
		// // a random number between 1 and 6.
		loadProperties(die);
		setDiscreteProb();
		Set<Integer> set = discreteProb.keySet();

		int[] numsToGenerate = new int[set.size()];
		double[] discreteProbabilities = new double[set.size()];

		int index = 0;

		for (Integer i : set) {
			numsToGenerate[index] = i;
			discreteProbabilities[index] = discreteProb.get(i);
			index++;
		}

		EnumeratedIntegerDistribution distribution = new EnumeratedIntegerDistribution(numsToGenerate,
				discreteProbabilities);

		int[] samples = distribution.sample(rollingTime);

		System.out.println("Die" + die + " settings: ");
		System.out.println("\tFavored face: " + favoredFace);
		System.out.println("\tFactor: " + factor);
		return samples;
	}

	private void setDiscreteProb() {
		double[] prob = getProb();
		for (int i = 1; i < 7; i++) {
			discreteProb.put(i, i == favoredFace ? prob[0] : prob[1]);

		}
		System.out.println(discreteProb.toString());
	}

	/**
	 * This method is solving the equations
	 * x = factor * y and x + 5y = 1 where x,y is the probability of favored and normal face respectively
	 * @return the normal face probability only
	 */
	private double[] getProb() {
		RealMatrix coefficients = new Array2DRowRealMatrix(new double[][] { { 1, factor * -1 }, { 1, 5 } }, false);
		DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();
		RealVector constants = new ArrayRealVector(new double[] { 0, 1 }, false);
		RealVector solutions = solver.solve(constants);
		return solutions.toArray();
	}

	public int getRollingTime() {
		return rollingTime;
	}

	public void setRollingTime(int rollingTime) {
		this.rollingTime = rollingTime;
	}

	public int getDie1() {
		return die1;
	}

	public int getDie2() {
		return die2;
	}

	public int getTotal() {
		return die1 + die2;
	}
}