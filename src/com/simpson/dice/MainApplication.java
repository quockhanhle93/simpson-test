package com.simpson.dice;
/**
 * @author quockhanhle93
 *
 */
public class MainApplication {

	public static void main(String[] args) {

		PairOfDice pairOfDice = new PairOfDice();
		int[] die1 = pairOfDice.roll(1);
		int[] die2 = pairOfDice.roll(2);
		System.out.println("===========================\nNummber of rolling time: " + pairOfDice.getRollingTime());
		System.out.println("---------------------------\nID\tDie1\tDie2\tSum\n---------------------------");

		for (int i = 0; i < die1.length; i++) {
			int sum = die1[i] + die2[i];
			int id = i + 1;
			System.out.println(id + "\t " + die1[i] + "\t " + die2[i] + "\t " + sum);
		}
	}
}
