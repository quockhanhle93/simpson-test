package com.simpson.utils;
/**
 * @author quockhanhle93
 *
 */
public class Constants {
	public static String PROPERTIES_FILENAME = "dice.properties";

	public static final String DIE1_FAVORED_FACE = "die1.favored.face";
	public static final String DIE1_FAVORED_FACTOR = "die1.favored.factor";
	public static final String DIE2_FAVORED_FACE = "die2.favored.face";
	public static final String DIE2_FAVORED_FACTOR = "die2.favored.factor";
	public static final String ROLLING_TIME = "rolling.time";

}