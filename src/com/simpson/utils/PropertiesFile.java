package com.simpson.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * PropertyFile Class 
 * Description: The PropertyFile class is used to parse
 * properties in setting file. property lines are parsed and converted into
 * properties and stored within this object.
 *
 * @author quockhanhle93
 */
public class PropertiesFile extends Properties {
	private static final long serialVersionUID = 1L;

	private void parseStream(BufferedReader br) throws IOException {
		String rawLine = null;

		while ((rawLine = br.readLine()) != null) {
			String Line = rawLine.trim();

			// ignore empty lines
			if (Line.length() == 0) {
				continue;
			}

			char ch = Line.charAt(0);

			// ignore comment lines
			if (ch == '#') {
				continue;
			}

			int i = Line.indexOf('=');
			if (i != -1) {
				char[] cArray = Line.toCharArray();
				String RawTagName = new String(cArray, 0, i);
				String RawValue = new String(cArray, i + 1, Line.length() - i - 1);
				setProperty(RawTagName.trim(), RawValue.trim());
			} else {
				setProperty(Line, "");
			}
		} // while
	}

	public void addProperties(String Filename) throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(Filename)));
		parseStream(br);
		br.close();
	}

	public PropertiesFile(String Filename) throws FileNotFoundException, IOException {
		addProperties(Filename);
	}

	public PropertiesFile() {
		super();
	}
}
