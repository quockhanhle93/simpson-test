package com.simpson.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author quockhanhle93
 *
 */
public class PropertiesUtils {
	private static final Properties properties = getProperties();

	private PropertiesUtils() {
		super();
	}

	private static PropertiesFile getProperties() {
		// create default properties in case not defined in dice.properties file
		PropertiesFile properties = new PropertiesFile();

		properties.setProperty(Constants.DIE1_FAVORED_FACE, "1");
		properties.setProperty(Constants.DIE1_FAVORED_FACTOR, "1");

		properties.setProperty(Constants.DIE2_FAVORED_FACE, "1");
		properties.setProperty(Constants.DIE2_FAVORED_FACTOR, "1");

		properties.setProperty(Constants.ROLLING_TIME, "10");

		File propertiesFile = new File(Constants.PROPERTIES_FILENAME);

		try {
			properties.load(new FileInputStream(propertiesFile.getPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return properties;
	}

	public static Properties getInstance() {
		return properties;
	}
}
